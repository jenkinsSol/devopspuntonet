﻿using System.Web;
using System.Web.Optimization;

namespace slnPlantillaMVC
{
    public class BundleConfig
    {
        public static void RegisterBundles(BundleCollection bundles)
        {
            /******************************/
            /* BUNDLES DE HOJAS DE ESTILO */
            /******************************/
            bundles.Add(new StyleBundle("~/Content/css").Include(
                "~/Content/style-responsive.css",
                "~/Content/sura_template.css"
            ));

            bundles.Add(new StyleBundle("~/Content/bootstrapcss").Include(
                "~/Content/bootstrap.css",
                "~/Content/bootstrap-theme.css",
                "~/Content/bootstrap-cards.css"
            ));

            bundles.Add(new StyleBundle("~/Content/datatablecss").Include(
                "~/Content/DataTables/DataTables-1.10.18/css/dataTables.bootstrap.css",
                "~/Content/DataTables/AutoFill-2.3.0/css/autoFill.bootstrap.min.css",
                "~/Content/DataTables/Buttons-1.5.2/css/buttons.bootstrap.css",
                "~/Content/DataTables/ColReorder-1.5.0/css/colReorder.bootstrap.css",
                "~/Content/DataTables/FixedColumns-3.2.5/css/fixedColumns.bootstrap.css",
                "~/Content/DataTables/FixedHeader-3.1.4/css/fixedHeader.bootstrap.css",
                "~/Content/DataTables/KeyTable-2.4.0/css/keyTable.bootstrap.css",
                "~/Content/DataTables/Responsive-2.2.2/css/responsive.bootstrap.css",
                "~/Content/DataTables/RowGroup-1.0.3/css/rowGroup.bootstrap.css",
                "~/Content/DataTables/RowReorder-1.2.4/css/rowReorder.bootstrap.css",
                "~/Content/DataTables/Scroller-1.5.0/css/scroller.bootstrap.css",
                "~/Content/DataTables/Select-1.2.6/css/select.bootstrap.css"
            ));

            bundles.Add(new StyleBundle("~/Content/sweetalertcss").Include(
                "~/Content/sweetalert.css"
            ));

            /**********************/
            /* BUNDLES DE SCRIPTS */
            /**********************/
            bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
                        "~/Scripts/jquery-{version}.js"
            ));

            bundles.Add(new ScriptBundle("~/bundles/jqueryval").Include(
                        "~/Scripts/jquery.unobtrusive*",
                        "~/Scripts/jquery.validate*"
            ));

            bundles.Add(new ScriptBundle("~/bundles/modernizr").Include(
                        "~/Scripts/modernizr-*"
            ));

            bundles.Add(new ScriptBundle("~/bundles/bootstrap").Include(
                    "~/Scripts/bootstrap.js"
            ));

            bundles.Add(new ScriptBundle("~/bundles/datatable").Include(
                "~/Content/DataTables/DataTables-1.10.18/js/jquery.dataTables.js",
                "~/Content/DataTables/DataTables-1.10.18/js/dataTables.bootstrap.js",
                "~/Content/DataTables/AutoFill-2.3.0/js/dataTables.autoFill.js",
                "~/Content/DataTables/Buttons-1.5.2/js/dataTables.buttons.js",
                "~/Content/DataTables/Buttons-1.5.2/js/buttons.colVis.js",
                "~/Content/DataTables/Buttons-1.5.2/js/buttons.flash.js",
                "~/Content/DataTables/Buttons-1.5.2/js/buttons.html5.js",
                "~/Content/DataTables/Buttons-1.5.2/js/buttons.print.js",
                "~/Content/DataTables/ColReorder-1.5.0/js/dataTables.colReorder.js",
                "~/Content/DataTables/FixedColumns-3.2.5/js/dataTables.fixedColumns.js",
                "~/Content/DataTables/FixedHeader-3.1.4/js/dataTables.fixedHeader.js",
                "~/Content/DataTables/KeyTable-2.4.0/js/dataTables.keyTable.js",
                "~/Content/DataTables/Responsive-2.2.2/js/dataTables.responsive.js",
                "~/Content/DataTables/Responsive-2.2.2/js/responsive.bootstrap.js",
                "~/Content/DataTables/RowGroup-1.0.3/js/dataTables.rowGroup.js",
                "~/Content/DataTables/RowReorder-1.2.4/js/dataTables.rowReorder.js",
                "~/Content/DataTables/Scroller-1.5.0/js/dataTables.scroller.js",
                "~/Content/DataTables/Select-1.2.6/js/dataTables.select.js"
            ));

            bundles.Add(new ScriptBundle("~/bundles/sweetalert").Include(
                "~/Scripts/sweetalert.js"
            ));

        }
    }
}