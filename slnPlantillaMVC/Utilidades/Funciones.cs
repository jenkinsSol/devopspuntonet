﻿using System;
using System.Web;
using WCFAsesuisa.ServiciosAuditoria;
using System.Diagnostics;
using System.Reflection;
using System.Web.Configuration;
using System.Xml.Serialization;
using System.Text;
using System.IO;
using System.Collections.Generic;

namespace slnPlantillaMVC.Utilidades
{
    public class Funciones
    {
        /// <summary>
        /// Metodo para guardar en Auditoria los errores 
        /// </summary>
        /// <param name="ex">Excepcion a auditar</param>
        /// <returns>string con la respuesta del servicio de auditoria</returns>
        public static string AuditarError(Exception ex)
        {
            try
            {
                Auditoria sAudita = new Auditoria(); //para registrar acciones, servicios y errores
                
                int IdUsuario = 1;
                int IdAplicacion = 1;
                string IpEquipo = "";
                string NombreEquipo = "";                 
                string NombreRecurso = "";
                string Clase = "";

                string TipoExcepcion = ex.GetType().ToString();
                string Error = ex.ToString();
                
                StackTrace trace = new StackTrace(StackTrace.METHODS_TO_SKIP + 1);
                StackFrame frame = trace.GetFrame(0);
                MethodBase caller = frame.GetMethod();

                if (caller != null) {
                    Clase = caller.DeclaringType.Name + '.' + caller.Name;                  
                }

                if (HttpContext.Current.Session["IdUsuario"] != null)
                {
                    IdUsuario = Convert.ToInt32(HttpContext.Current.Session["IdUsuario"].ToString());
                }

                if (HttpContext.Current.Session["IdAplicacion"] != null)
                {
                    IdAplicacion = Convert.ToInt32(HttpContext.Current.Session["IdAplicacion"].ToString());
                }

                if (HttpContext.Current.Session["MyIP"] != null)
                {
                    IpEquipo = HttpContext.Current.Session["MyIP"].ToString();
                }

                if (HttpContext.Current.Session["HostName"] != null)
                {
                    NombreEquipo = HttpContext.Current.Session["HostName"].ToString();
                }

                if (HttpContext.Current.Session["RutaRecurso"] != null)
                {
                    NombreRecurso = HttpContext.Current.Session["RutaRecurso"].ToString();
                }

                return sAudita.AuditaErrores(IdUsuario, IdAplicacion, TipoExcepcion, NombreRecurso, IpEquipo, NombreEquipo, Clase, Error);
            }
            catch (Exception ex2)
            {
                return "No se logro auditar el error: " + ex2.Message;
            }
        }

        /// <summary>
        /// Metodo para guardar en Auditoria las acciones
        /// </summary>
        /// <param name="accion">Accion a auditar</param>
        /// <param name="queryUsado">Informacion adicional que pueda ser util</param>
        /// <returns>Boolean que indica si fue o no exitosamente auditado</returns>       
        public static bool AuditarAccion(string accion, string queryUsado)
        {
            try
            {
                Auditoria sAudita = new Auditoria(); //para registrar acciones, servicios y errores

                int IdUsuario = 1;
                int IdAplicacion = 1;
                string NombreRecurso = "N/A";

                if (HttpContext.Current.Session["IdUsuario"] != null)
                {
                    IdUsuario = Convert.ToInt32(HttpContext.Current.Session["IdUsuario"].ToString());
                }

                if (HttpContext.Current.Session["IdAplicacion"] != null)
                {
                    IdAplicacion = Convert.ToInt32(HttpContext.Current.Session["IdAplicacion"].ToString());
                }

                return sAudita.AuditaAcciones(IdUsuario, IdAplicacion, NombreRecurso, accion, queryUsado);
            }
            catch (Exception ex)
            {
                AuditarError(ex);
                return false;
            }
        }

        /// <summary>
        /// Consulta una llave del archivo de configuración (String)
        /// </summary>
        /// <param name="key">Llave a consultar</param>
        /// <returns>String con el valor de la llave consultada</returns>
        public static string ClaveWebConfig(string key)
        {
            try
            {
                return WebConfigurationManager.AppSettings[key].ToString();
            }
            catch
            {
                return "";
            }
        }

        /// <summary>
        /// Consulta una llave del archivo de configuración (Entero)
        /// </summary>
        /// <param name="key">Llave a consultar</param>
        /// <returns>Entero con el valor de la llave consultada</returns>
        public static int ClaveWebConfigInt(string key)
        {
            try
            {
                return Convert.ToInt32(WebConfigurationManager.AppSettings[key].ToString());
            }
            catch
            {
                return 0;
            }
        }


        public static string SerializarXML<T>(object objeto)
        {
            try
            {                
                XmlSerializer serializador = new XmlSerializer(typeof(T));
                StringBuilder sb = new StringBuilder();
                TextWriter tw = new StringWriter(sb);
                serializador.Serialize(tw, objeto);
                tw.Close();
                string xml = sb.ToString();
                return xml;
            }
            catch (Exception ex)
            {
                AuditarError(ex);
                return "Error al serializar xml " + ex.Message;
            }
        }
    }
}