﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace slnPlantillaMVC.Utilidades
{
    public class Respuesta
    {
        public string codigo { get; set; }
        public string Descripcion { get; set; }

        public Respuesta()
        {
            codigo = "00";
            Descripcion = "";
        }

        public Respuesta(string _codigo, string _descripcion)
        {
            codigo = _codigo;
            Descripcion = _descripcion;
        }
    }
}