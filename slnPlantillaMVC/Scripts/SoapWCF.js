﻿/***********************************************
Nombre:         SoapWCF.js
Usuario Creo:   eurbina
Fecha Creacion: 01/11/2018
Descripcion:    Este archivo contiene los script para el ejemplo de consumo SOAP

/********************************************
 Variables globales
 ********************************************/

//Modulo principal
(function (w, d, $) {
    w.soap = {
        init: function () {
            setEvents();
        },
        ConsultaMunicipios: ConsultaMunicipios,
        ConsultaColonias: ConsultaColonias,
        AgregarParametro: AgregarParametro,
        ActualizaParametro: ActualizaParametro
    };

    function setEvents() {
        console.log('Inicializar eventos modulo SOAP');

        $("#btn-entero").click(function () {
            var depto = $("#txtIdDepto2").val();
            ConsultaMunicipios(depto);
        });        

        
        $("#btn-enteros").click(function () {
            var depto = $("#txtIdDepto1").val();
            var muni = $("#txtIdMunicipio").val();
            ConsultaColonias(depto, muni);
        });
        
        $("#btn-agregar").click(function () {
            var Desc = $("#txtDescParam").val();
            var Fecha = $("#txtFechaParam").val();
            var Num = $("#txtNumParam").val();
            var Texto = $("#txtTextoParam").val();
            var Porc = $("#txtPorcParam").val();
            var Imp = $("#txtImpParam").val();

            AgregarParametro(Desc, true, Fecha, Num, Texto, Porc, Imp);
        });

        $("#btn-actualizar").click(function () {
            var Id = $("#txtIdParam").val();
            var Desc = $("#txtDescParam2").val();
            var Fecha = $("#txtFechaParam2").val();
            var Num = $("#txtNumParam2").val();
            var Texto = $("#txtTextoParam2").val();
            var Porc = $("#txtPorcParam2").val();
            var Imp = $("#txtImpParam2").val();

            ActualizaParametro(Id, Desc, true, Fecha, Num, Texto, Porc, Imp);

        });       
    };

    function ConsultaMunicipios(idDepto, callback) {
        var params = { idDepto: idDepto }

        LlamadaAjax(URL_1, params, "text")
            .done(function (data) {
                $("#pre-entero").text(data);               
                if (callback) { callback(); }
            })
            .fail(function (xhr, ajaxOptions, thrownError) {
                $("#pre-entero").text(thrownError);
            });
    };

    
    function ConsultaColonias(idDepto, idMunicipio, callback) {
        var params = {
            departamento: idDepto,
            municipio: idMunicipio
        }

        LlamadaAjax(URL_2, params, "text")
            .done(function (data) {
                $("#pre-enteros").text(data);
                if (callback) { callback(); }
            })
            .fail(function (xhr, ajaxOptions, thrownError) {
                $("#pre-enteros").text(thrownError);
            });
    };

    function AgregarParametro(descripcion, esParam, fecha, numero, texto, porcentaje, importe, callback) {
        var params = {
            descripcion: descripcion,
            esParam: esParam,
            fecha: fecha,
            numero: numero,
            texto: texto,
            porcentaje: porcentaje,
            importe: importe
        }

        LlamadaAjax(URL_3, params, "text")
            .done(function (data) {
                $("#pre-agregar").text(data);
                if (callback) { callback(); }
            })
            .fail(function (xhr, ajaxOptions, thrownError) {
                $("#pre-agregar").text(thrownError);
            });
    };


    function ActualizaParametro(id, descripcion, esParam, fecha, numero, texto, porcentaje, importe, callback) {
        var params = {
            id: id,
            descripcion: descripcion,
            esParam: esParam,
            fecha: fecha,
            numero: numero,
            texto: texto,
            porcentaje: porcentaje,
            importe: importe
        }

        LlamadaAjax(URL_4, params, "text")
            .done(function (data) {
                $("#pre-actualizar").text(data);
                if (callback) { callback(); }
            })
            .fail(function (xhr, ajaxOptions, thrownError) {
                $("#pre-actualizar").text(thrownError);
            });
    };

    $(d).ready(soap.init);
})(window, document, jQuery);