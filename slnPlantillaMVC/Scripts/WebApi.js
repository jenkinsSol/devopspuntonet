﻿/***********************************************
Nombre:         WebApi.js
Usuario Creo:   eurbina
Fecha Creacion: 15/10/2018
Descripcion:    Este archivo contiene los script para el Ejemplo de Consumo REST

/********************************************
 Variables globales
 ********************************************/

//Modulo principal
(function (w, d, $) {
    w.webapi = {
        init: function () {
            setEvents();
        },
        ConsultaMunicipios: ConsultaMunicipios,
        ConsultaColonias: ConsultaColonias,
        AgregarParametro: AgregarParametro,
        ActualizaParametro: ActualizaParametro
    };

    function setEvents() {
        console.log('Inicializar eventos modulo WebApi');

        $("#btn-get-params").click(function () {
            var depto = $("#txtIdDepto1").val();
            var muni = $("#txtIdMunicipio").val();
            ConsultaColonias(depto, muni);
        });

        $("#btn-get-id").click(function () {
            var depto = $("#txtIdDepto2").val();
            ConsultaMunicipios(depto);
        });        

        $("#btn-post").click(function () {
            var Desc = $("#txtDescParam").val();
            var Fecha = $("#txtFechaParam").val();
            var Num = $("#txtNumParam").val();
            var Texto = $("#txtTextoParam").val();
            var Porc = $("#txtPorcParam").val();
            var Imp = $("#txtImpParam").val();

            AgregarParametro(Desc, true, Fecha, Num, Texto, Porc, Imp);
        });

        $("#btn-put").click(function () {
            var Id = $("#txtIdParam").val();
            var Desc = $("#txtDescParam2").val();
            var Fecha = $("#txtFechaParam2").val();
            var Num = $("#txtNumParam2").val();
            var Texto = $("#txtTextoParam2").val();
            var Porc = $("#txtPorcParam2").val();
            var Imp = $("#txtImpParam2").val();

            ActualizaParametro(Id, Desc, true, Fecha, Num, Texto, Porc, Imp);

        }); 
    };

    function ConsultaMunicipios(idDepto, callback) {
        var params = { idDepto: idDepto }

        LlamadaAjax(URL_1, params)
            .done(function (data) {
                $("#pre-get-id").text(data);
                if (callback) { callback(); }
            })
            .fail(function (xhr, ajaxOptions, thrownError) {
                $("#pre-get-id").text(thrownError);
            });
    };

    function ConsultaColonias(idDepto, idMunicipio, callback) {
        var params = {
            departamento: idDepto,
            municipio: idMunicipio
        }

        LlamadaAjax(URL_2, params)
            .done(function (data) {
                $("#pre-get-params").text(data);
                if (callback) { callback(); }
            })
            .fail(function (xhr, ajaxOptions, thrownError) {
                $("#pre-get-params").text(thrownError);
            });
    };

    function AgregarParametro(descripcion, esParam, fecha, numero, texto, porcentaje, importe, callback) {
        var params = {
            descripcion: descripcion,
            esParam: esParam,
            fecha: fecha,
            numero: numero,
            texto: texto,
            porcentaje: porcentaje,
            importe: importe
        }

        LlamadaAjax(URL_3, params)
            .done(function (data) {
                $("#pre-post").text(data);
                if (callback) { callback(); }
            })
            .fail(function (xhr, ajaxOptions, thrownError) {
                $("#pre-post").text(thrownError);
            });
    };


    function ActualizaParametro(id, descripcion, esParam, fecha, numero, texto, porcentaje, importe, callback) {
        var params = {
            id: id,
            descripcion: descripcion,
            esParam: esParam,
            fecha: fecha,
            numero: numero,
            texto: texto,
            porcentaje: porcentaje,
            importe: importe
        }

        LlamadaAjax(URL_4, params)
            .done(function (data) {
                $("#pre-put").text(data);
                if (callback) { callback(); }
            })
            .fail(function (xhr, ajaxOptions, thrownError) {
                $("#pre-put").text(thrownError);
            });
    };
    $(d).ready(webapi.init);
})(window, document, jQuery);