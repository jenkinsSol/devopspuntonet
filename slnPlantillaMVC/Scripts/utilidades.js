﻿/**
 * Funcion para hacer una llamada ajax generica
 * Por defecto es una llamada asincrona
 * Se le puede pasar por parametro una funcion 'antes' 
 * que se ejecuta antes que la llamada ajax ocurra
 *
 * Ejemplo de consumo:
 *         LlamadaAjax(URL_1, params)
 *         .done(function (data) {
 *             // Codigo para cuando la ejecuion es exitosa
 *         })
 *         .fail(function (xhr, ajaxOptions, thrownError) {
 *             // Codigo para cuando la ejecucion falla 
 *         });
 */
function LlamadaAjax(url, parametros, tipoDataRespuesta, antes) {
    return $.ajax({
        "async": true,
        "method": "POST",
        "crossDomain": true,
        "headers": {
            "content-type": "application/json",
            "cache-control": "no-cache"
        },
        "url": url,
        "processData": false,
        "data": JSON.stringify(parametros),
        "dataType": tipoDataRespuesta ? tipoDataRespuesta : "json",
        "beforeSend": function () { if (antes) { antes(); } }
    });
}

/**
 * Funciones para levantar la pantalla de carga cuando se inicie una peticion Ajax
 * Funciones para ocultar la pantalla de carga cuando termine una peticion Ajax
 */
$(document).ajaxStart(loadingInit);

$(document).ajaxStop(loadingEnd);

function loadingInit() {
    $('#Loading').css("display", "inline-grid");
}

function loadingEnd() {
    $('#Loading').css("display", "none");
}

/*
 * Funcion que da el formato de moneda a un numero ingresado
 * Redondea a 2 decimales,
 * Coloca el separador de miles
 * Coloca el simbolo de dolar ($)
 */
function formatoMoneda(total) {
    var neg = false;
    if (total < 0) {
        neg = true;
        total = Math.abs(total);
    }
    return (neg ? "-$ " : '$ ') + parseFloat(total, 10).toFixed(2).replace(/(\d)(?=(\d{3})+\.)/g, "$1,").toString();
}


/*
 * Funcion para eliminar el formato de moneda
 * para obtener datos el valor numerico
 * Elimina el simbolo de dolar
 * Elimina el separador de miles
 */
function valorNumerico(i) {
    return typeof i === 'string' ?
        i.replace(/[\$,]/g, '') * 1 :
        typeof i === 'number' ?
            i : 0;
}

/*
 * Funcion para buscar un elemento en una lista o array
 * haciendo la busqueda por nombre del campo y el valor a comparar
 */
function buscarElemento(lista, campo, valor) {

    if (lista != null) {
        for (var i = 0; i < lista.length; i++) {
            if (lista[i][campo] == valor) {
                return lista[i];
            }
        }
    }
    return -1;
}

/**
 * Funcion para obtener el valor seleccionado en un combobox 
 * El parametro 'type' determina lo que se devolvera
 *     'val' para el value
 *     'text' para el texto
 */
function ValorSeleccionado(obj, type) {
    if (type == 'val') {
        return $('#' + obj).find("option:selected").val();
    }
    else {
        return $('#' + obj).find("option:selected").text();
    }
}

/*
 * Retorna si un checkbox esta marcado o 
 * devuelve falso o verdadero
 */
function Chekeado(obj) {
    return $('#' + obj).prop('checked') ? true : false
}


/**
 * Funcion para validar el correo electronico
 */
function validarFormatoCorreo(correo) {
    var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(String(correo).toLowerCase());
}

/**
 * Funcion para convertir Lista a Array
 */
function ListHashToArray(list) {
    var array = new Array();

    $.each(list, function () {
        var _row = [];
        $.each(this, function () {
            _row[this.Key] = this.Value;
        });
        array.push(_row);
    });

    return array;
}


/**
 * Funcion para obtener la fecha actual
 */
function fechaActual() {
    var hoy = new Date();
    return (hoy.getDate() < 10 ? "0" : "") + hoy.getDate()
        + "/" +
        ((hoy.getMonth() + 1) < 10 ? "0" : "") + (hoy.getMonth() + 1)
        + "/" +
        hoy.getFullYear();
}



/**
 * Funciones pre-armadas para la libreria Sweet Alert
 */
function MostrarMensajeExito(msj) {
    swal("", msj ? msj: "Operación exitosa!", "success");    
}

function MostrarMensajeInfo(msj) {
    swal("", msj ? msj : "Operación exitosa!", "info");
}

function MostrarMensajeError(msj) {
    swal('', msj ? msj : 'Ocurrió un error con la petición al servidor intente nuevamente, o comuníquese a la mesa de servicio ', "error");
}

function MostrarMensajeAdvertencia(msj) {
    swal(
        {title: "",
        text: msj ? msj: "Advertencia!",
        type: "warning", 
        showCancelButton: true,
        showConfirmButton: false,
        cancelButtonText: 'Aceptar'
    });
}

function mensajeConfirmacion(titulo, mensaje, tipo, textoCancelar, textoAceptar, callback) {
    swal({
        title: titulo,
        text: mensaje,
        type: tipo,
        showCancelButton: true,
        confirmButtonClass: "btn-" + tipo,
        confirmButtonText: textoAceptar,
        cancelButtonText: textoCancelar,
        closeOnConfirm: true
    },
        function () {
            if (callback) { callback(); }
        });
}

/**
 * Funcion para obtener un parametro especifico de la URL
 */
function getUrlVars(nameParam) {
    var vars = [], hash;
    var resp = "";

    var hashes = window.location.href.slice(window.location.href.indexOf('?') + 1).split('&');
    for (var i = 0; i < hashes.length; i++) {
        hash = hashes[i].split('=');
        vars.push(hash[0]);
        vars[hash[0]] = hash[1];
    }

    if (vars.length>0) {
        resp = vars[nameParam] == null ? "" : vars[nameParam];
    }

    return resp;
}

/**
 * Script para expandir y contraer la barra de navegacion
 */
(function ($) {
    $(window).scroll(function () {
        if ($(window).scrollTop() > 50) {
            $('header').addClass('header_min');
            $('header').removeClass('header_max');
        } else {
            $('header').removeClass('header_min');
            $('header').addClass('header_max');
        }
    });
})(jQuery);

/**
 * Funcion para deshabilitar el boton retroceso
 * Agrega los simbolos #! al final de la url 
 */

// (function (global) {

//     if (typeof (global) === "undefined") {
//         throw new Error("window is undefined");
//     }

//     var _hash = "!";
//     var noBackPlease = function () {
//         global.location.href += "#";

//         global.setTimeout(function () {
//             global.location.href += "!";
//         }, 50);
//     };

//     global.onhashchange = function () {
//         if (global.location.hash !== _hash) {
//             global.location.hash = _hash;
//         }
//     };

//     global.onload = function () {
//         noBackPlease();

//         // disables backspace on page except on input fields and textarea..
//         document.body.onkeydown = function (e) {
//             var elm = e.target.nodeName.toLowerCase();
//             if (e.which === 8 && (elm !== 'input' && elm !== 'textarea')) {
//                 e.preventDefault();
//             }
//             // stopping event bubbling up the DOM tree..
//             e.stopPropagation();
//         };
//     }

// })(window);