﻿/***********************************************
Nombre:         main.js
Usuario Creo:   xxxxx
Fecha Creacion: 26/10/2018
Descripcion:    Este archivo contiene los script para la pagina principal
***********************************************/

/********************************************
 Variables globales
 ********************************************/

/******************************************/

// Modulo principal
(function (w, d, $) {
    w.main = {
        init: function () {
            /*
             * Inicializar controles de esta sección
             *
             *
             */


            // Inicializar los otros modulos:
            Asegurado.init();
            Asesor.init();

            setEvents();
        }
    };

  function  setEvents(){
        console.log('Inicializando eventos del modulo Main');
    }

    $(d).ready(main.init);
})(window, document, jQuery);


// Modulo para información del asegurado
(function (w, d, $) {
    w.Asegurado = {
        init: function () {
            /*
             * Inicializar controles de esta sección
             *
             *
             */

            setEvents();
        }

    };

    function setEvents() {
        console.log('Inicializando eventos del modulo Asegurado');
    }

})(window, document, jQuery);


// Modulo para información del Asesor
(function (w, d, $) {
    w.Asesor = {
        init: function () {
            /*
             * Inicializar controles de esta sección
             *
             *
             */

            setEvents();
        }

    };

    function setEvents() {
        console.log('Inicializando eventos del modulo Asesor');
    }

})(window, document, jQuery);
