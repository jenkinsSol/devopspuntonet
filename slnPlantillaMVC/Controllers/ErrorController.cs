﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace slnPlantillaMVC.Controllers
{
    public class ErrorController : Controller
    {
        public ActionResult Index(int error, string mensaje = "")
        {
            ViewBag.Title = "Error";
            switch (error)
            {
                case 500:
                    ViewBag.TituloError = "Ocurrio un error inesperado";
                    ViewBag.Description = "Esto es muy vergonzoso, esperemos que no vuelva a pasar...";
                    break;

                case 401:
                    ViewBag.TituloError = "Credenciales incorrectas";
                    ViewBag.Description = "Debe volver a loguearse en el portal de aplicaciones";
                    break;

                case 403:
                    ViewBag.TituloError = "No autorizado";
                    ViewBag.Description = "No tiene permisos para ingresar a la pagina solicitada";
                    break;

                case 404:
                    ViewBag.TituloError = "Página no encontrada";
                    ViewBag.Description = "La URL que está intentando ingresar no existe";
                    break;

                default:
                    ViewBag.TituloError = "Error";
                    ViewBag.Description = "Ocurrio un error en el proceso";
                    break;
            }

            if (!mensaje.Equals(""))
            {
                ViewBag.Descripcion = mensaje;
            }

            return View("~/Views/Shared/_Error.cshtml");
        }
    }
}